export class FormatDateService {
  static monthNamesCorrespondingNumber = {
    Jan: "01",
    Feb: "02",
    Mar: "03",
    Apr: "04",
    May: "05",
    Jun: "06",
    Jul: "07",
    Aug: "08",
    Sep: "09",
    Oct: "10",
    Nov: "11",
    Dec: "12",
  };

  static createCurrentDate() {
    return new Date();
  }

  static createYesterdayDate() {
    let date = new Date();
    date.setDate(date.getDate() - 1);
    return date;
  }

  static extractDateData(date) {
    date = String(date);
    date = date.split(" ");
    date = date.slice(1, 4);
    date.push("01:00:00");
    return date;
  }

  static formatMonthDate(date) {
    const month = this.monthNamesCorrespondingNumber;
    date[0] = month[date[0]];
    return date;
  }

  static formatDate(date) {
    date = date.join(" ");
    return date;
  }

  static formatDateToTimestamp(date) {
    return Date.parse(date) / 1000;
  }
}
