import { PageService } from "./PageService.js";

export class MintscanService extends PageService {
  static async getDataFromPage(page) {
    return await page.evaluate(() => {
      let name;
      let inflation;
      let stacking_apr;
      let price;
      const regexForNumber = /^[^a-zA-Z]+$/;

      try {
        name = document.querySelector(
          "div.rel.s-VWN8CcImG9q- > div > div > div > div > div > div:nth-child(1) > div > div > div > div > div.root.s-B5fp-zeMUDH- > div > div > div > div > div > div"
        ).innerText;
      } catch (e) {
        console.log("can't get asset name on mintscan");
      }
      try {
        inflation = document.querySelector(
          "div.contents.s-AlA33PZP3RDs > section > div > div > div:nth-child(5) > div > div > div > div:nth-child(2) > div > div:nth-child(2) > div"
        ).innerText;
      } catch (e) {
        console.log("can't get asset inflation on mintscan");
      }
      try {
        stacking_apr = document.querySelector(
          "div.contents.s-AlA33PZP3RDs > section > div > div > div:nth-child(6) > div > div > div > div:nth-child(2) > div > div:nth-child(2) > div"
        ).innerText;
      } catch (e) {
        console.log("can't get asset stacking_apr on mintscan");
      }
      try {
        price = document.querySelector(
          "div.contents.s-AlA33PZP3RDs > div > div:nth-child(1) > div > div > div:nth-child(1) > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div"
        ).innerText;
      } catch (e) {
        console.log("can't get asset price on mintscan");
      }

      return {
        name: name ? name : "can't get asset name on mintscan",
        price:
          price && regexForNumber.test(price)
            ? price
            : "can't get asset price on mintscan",
        inflation:
          inflation && regexForNumber.test(inflation)
            ? inflation
            : "can't get asset inflation on mintscan",
        stacking_apr:
          stacking_apr && regexForNumber.test(stacking_apr)
            ? stacking_apr
            : "can't get asset stacking_apr on mintscan",
      };
    });
  }

  static async getDataFromPageAsset(page) {
    return await page.evaluate(() => {
      let tvl;
      try {
        tvl = document.querySelector(
          "#content-root > div.root.s-ZNy69WCvR5Me > div > div > div:nth-child(1) > div > div.contents.s-AlA33PZP3RDs > section > div > div > div:nth-child(1) > div:nth-child(4)"
        ).innerText;
      } catch (e) {
        console.log("can't get asset tvl on page asset of mintscan");
      }
      return tvl ? tvl : "can't get asset tvl on page asset of mintscan";
    });
  }

  static async getDataFromPageVisualization(page) {
    return await page.evaluate(() => {
      let tmc;
      try {
        tmc = document.querySelector(
          "#content-root > div > div:nth-child(1) > div > div:nth-child(3) > section > div > div > div:nth-child(2) > div.tag-anim.s-dpAAwU3LLMp_ > div.text.s-dpAAwU3LLMp_ > div"
        ).innerText;
      } catch (e) {
        console.log("can't get asset total market cap on mintscan");
      }
      return tmc ? tmc : "can't get asset total market cap on mintscan";
    });
  }

  static async getDataFromPageAddress(page) {
    return await page.evaluate(() => {
      let total_mev_revenus;
      try {
        total_mev_revenus = document.querySelector(
          "#content-root > div.root.s-ZNy69WCvR5Me > div > div.root.s-AlA33PZP3RDs > div.contents.s-AlA33PZP3RDs > div > div:nth-child(1) > div > div.contents.s-AlA33PZP3RDs > div > div:nth-child(3) > section > div > div > div > div:nth-child(2)"
        ).innerText;
      } catch (e) {
        console.log("can't get asset total mev revenus on mintscan");
      }
      return total_mev_revenus
        ? total_mev_revenus
        : "can't get asset total mev revenus on mintscan";
    });
  }
}
