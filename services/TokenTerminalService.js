import { PageService } from "./PageService.js";

export class TokenTerminalService extends PageService {
  static async getDataFromPageFields(page) {
    return await page.evaluate(() => {
      let fields_name;
      try {
        fields_name = Array.from(
          document.querySelectorAll(
            "._1cidn1cfx._1cidn1c7n > section > ol > li > p:nth-child(1)"
          )
        ).map((e) => {
          return e.innerText;
        });
      } catch (e) {
        console.log("can't get fields name from page fields on token terminal");
      }
      let fields_values;
      try {
        fields_values = Array.from(
          document.querySelectorAll(
            "._1cidn1cfx._1cidn1c7n > section > ol > li > span > p:nth-child(1)"
          )
        ).map((e) => {
          return e.innerText;
        });
      } catch (e) {
        console.log(
          "can't get fields values from page fields on token terminal"
        );
      }

      let fields = fields_name.map((e, i) => {
        return {
          name: e ? e : "can't get fields names",
          value: fields_values?.[i]
            ? fields_values[i]
            : "can't get fields values",
        };
      });

      return fields;
    });
  }

  static async getDataFromPageFieldsTable(page) {
    return await page.evaluate(() => {
      let fields_name;
      try {
        fields_name = Array.from(
          document.querySelectorAll(
            "#main-content > div > div._2rn9xx0._1cidn1ceb._1cidn1ceg._1cidn1cel._1cidn1ccz._1cidn1cd4._1cidn1cd9._1cidn1cgz._1cidn1ch4._1cidn1ch9._1cidn1cfn._1cidn1cfs._1cidn1cfx._1cidn1c3v._1cidn1crr._1cidn1c27._1cidn1c4v._1cidn1c8z.iv7w9e0.iv7w9e4.card._5bvu1x1._5bvu1x0 > div > div._2rn9xx0._1cidn1c23._1cidn1c15._1cidn1c6f._1cidn1c4b._12dzlxt8._1cidn1c3z._1cidn1cpr._12dzlxt7._126g82n9._1cidn1c53._126g82nb.jram8f1 > table > tbody._2rn9xx0.jram8fq > tr:nth-child(n)"
          )
        ).map((e) => {
          return e.innerText;
        });
      } catch (e) {
        console.log(
          "can't get fields name from page fields table on token terminal"
        );
      }

      let fields_values;
      try {
        fields_values = Array.from(
          document.querySelectorAll(
            "#main-content > div > div._2rn9xx0._1cidn1ceb._1cidn1ceg._1cidn1cel._1cidn1ccz._1cidn1cd4._1cidn1cd9._1cidn1cgz._1cidn1ch4._1cidn1ch9._1cidn1cfn._1cidn1cfs._1cidn1cfx._1cidn1c3v._1cidn1crr._1cidn1c27._1cidn1c4v._1cidn1c8z.iv7w9e0.iv7w9e4.card._5bvu1x1._5bvu1x0 > div > div._2rn9xx0._1cidn1c23._1cidn1c15._1cidn1c6f._1cidn1c4b._12dzlxt8._1cidn1c3z._1cidn1cpr._12dzlxt7._126g82n9._1cidn1c53._126g82nb.jram8f1 > table > tbody._2rn9xx0.jram8fq > tr:nth-child(n) > td:nth-child(2)"
          )
        ).map((e) => {
          return e.innerText;
        });
      } catch (e) {
        console.log(
          "can't get fields values from page fields table on token terminal"
        );
      }

      let fields = fields_name.map((e, i) => {
        return {
          name: e
            ? e
            : "can't get fields name from page fields table on token terminal",
          value: fields_values?.[i]
            ? fields_values[i]
            : "can't get fields values from page fields table on token terminal",
        };
      });

      return fields;
    });
  }
}
