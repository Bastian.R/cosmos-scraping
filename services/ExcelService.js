export class ExcelService {
  static createExcelFile(exceljs, asset) {
    let workbook = new exceljs.Workbook();
    let ws = workbook.addWorksheet(asset.name);
    return { workbook, ws };
  }

  static initializeColumns(worksheet, assetProperties) {
    let columnObjectTable = assetProperties.map((property) => {
      return { header: property, key: property, width: 30 };
    });
    worksheet.columns = columnObjectTable;
  }

  static initializeCells(worksheet, assetProperties, alphabet, asset) {
    assetProperties.forEach((property, index) => {
      worksheet.getCell(`${alphabet[index]}2`).value = asset[property];
    });
  }

  static stylingRows(worksheet) {
    worksheet.getRow(1).font = {
      size: 14,
      bold: true,
    };

    worksheet.getRow(2).font = {
      size: 14,
      bold: true,
    };
  }

  static initializeDate() {
    const date = new Date();
    const localDate = String(date.toLocaleDateString("fr-FR")).replaceAll(
      "/",
      "-"
    );
    return localDate;
  }

  static async saveExcelFile(workbook, asset, date) {
    return workbook.xlsx.writeFile(
      `./asset/excel/data_${asset.name}_${date}.xlsx`
    );
  }

  static extractColumnName(asset) {
    return Object.keys(asset);
  }
}
