import { PageService } from "./PageService.js";

export class MapOfZonesService extends PageService {
  static async getDataFromPageField(page) {
    return await page.evaluate(() => {
      let childs;
      const regexForNumber = /^[^a-zA-Z]+$/;
      try {
        childs = Array.from(
          document.querySelectorAll(
            ".ValueWithPending_valueContainer__ehTR7 > span > span"
          )
        ).map((e) => e.innerText);
      } catch (e) {
        console.log("can't get asset data from page field on map of zones");
      }
      if (childs) {
        return {
          ibc_volume:
            childs?.[0] && regexForNumber
              ? childs[0]
              : "can't get asset ibc_volume on map of zones",
          peers:
            childs?.[5] && regexForNumber
              ? childs[5]
              : "can't get asset peers on map of zones",
        };
      } else {
        return {
          ibc_volume: "can't get asset ibc_volume on map of zones",
          peers: "can't get asset peers on map of zones",
        };
      }
    });
  }
}
