import { PageService } from "./PageService.js";

export class AxelarService extends PageService {
  static async getDataFromPageInterchainGasFees(page) {
    return await page.evaluate(() => {
      let gasFees;
      try {
        gasFees = document.querySelector(
          "#__next > div > div.wrapper > div > div:nth-child(3) > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div:nth-child(4) > div > div > div:nth-child(2) > span"
        ).innerText;
      } catch (e) {
        console.log("can't get interchain gas fees on axelar");
      }
      return gasFees ? gasFees : "can't get interchain gas fees on axelar";
    });
  }

  static async getDataFromPageInterchain24h(page) {
    return await page.evaluate(() => {
      let volume_j_minus_1;
      let gasFees_j_minus_1;
      try {
        volume_j_minus_1 = document.querySelector(
          "#__next > div > div.wrapper > div > div:nth-child(3) > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div:nth-child(2) > div > div > div:nth-child(2) > span"
        ).innerText;
      } catch (e) {
        console.log("can't get interchain volume_j_minus_1 on axelar");
      }
      try {
        gasFees_j_minus_1 = document.querySelector(
          "#__next > div > div.wrapper > div > div:nth-child(3) > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div:nth-child(4) > div > div > div:nth-child(2) > span"
        ).innerText;
      } catch (e) {
        console.log("can't get interchain gasFees_j_minus_1 on axelar");
      }
      return {
        volume_j_minus_1: volume_j_minus_1
          ? volume_j_minus_1
          : "can't get interchain volume_j_minus_1 on axelar",
        gasFees_j_minus_1: gasFees_j_minus_1
          ? gasFees_j_minus_1
          : "can't get interchain gasFees_j_minus_1 on axelar",
      };
    });
  }
}
