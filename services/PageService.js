export class PageService {
  static async goToPage(page, url) {
    return await page.goto(url, {
      waitUntil: "domcontentloaded",
    });
  }

  static async waitForPageSelector(page, selector) {
    return await page.waitForSelector(selector);
  }

  static async clickOnPageElement(page, selector) {
    return await page.click(selector);
  }
}
