import { PageService } from "./PageService.js";

export class CoinMarketCapService extends PageService {
  static async getDataFromPageCurrencies(page) {
    return await page.evaluate(() => {
      let price;
      try {
        price = document.querySelector(
          "#section-coin-overview > div.sc-f70bb44c-0.flfGQp.flexStart.alignBaseline > span"
        ).innerText;
      } catch (e) {
        console.log(
          "can't get asset price on page currencies of coinmarketcap"
        );
      }

      let market_cap;
      try {
        market_cap = document
          .querySelector(
            "#section-coin-stats > div > dl > div:nth-child(1) > div.sc-f70bb44c-0.sc-26ba3ba-0.kgxbNk.flexBetween > dd"
          )
          .textContent.slice(5);
      } catch (e) {
        console.log(
          "can't get asset market cap on page currencies of coinmarketcap"
        );
      }

      let market_cap_diluted;
      try {
        market_cap_diluted = document.querySelector(
          "#section-coin-stats > div > dl > div:nth-child(7) > div > dd"
        ).innerText;
      } catch (e) {
        console.log(
          "can't get asset market cap fully diluted on page currencies of coinmarketcap"
        );
      }

      return {
        price: price
          ? price
          : "can't get asset price on page currencies of coinmarketcap",
        market_cap: market_cap
          ? market_cap
          : "can't get asset market cap on page currencies of coinmarketcap",
        market_cap_fully_diluted: market_cap_diluted
          ? market_cap_diluted
          : "can't get asset market cap fully diluted on page currencies of coinmarketcap",
      };
    });
  }
}
