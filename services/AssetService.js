export class AssetService {
  static formatTotalMarketCap(total_market_cap) {
    return total_market_cap
      .split("")
      .filter((e) => e != ",")
      .join("");
  }

  static formatMarketCap(market_cap) {
    return market_cap
      .split("")
      .slice(1)
      .filter((e) => e != ",")
      .join("");
  }

  static formatGasFees(gas_fees) {
    return gas_fees
      .split("")
      .slice(1)
      .filter((e) => e != ",")
      .join("");
  }

  static calculateMarketCapDominance(market_cap, total_market_cap) {
    return (Number(market_cap) / Number(total_market_cap)) * 100;
  }

  static formatMarketCapDominance(asset) {
    asset.market_cap_dominance = String(asset.market_cap_dominance);
    asset.market_cap_dominance = asset.market_cap_dominance
      .split("")
      .slice(0, 5)
      .join("");
    return (asset.market_cap_dominance += "%");
  }

  static initializeDataFromMapOfZonesPageField(asset, valueFromMapOfZones) {
    return { ...asset, ...valueFromMapOfZones };
  }

  static filterDataFromTokenTerminalPageFields(asset) {
    return asset.data_token_terminal.filter(
      (e) => e.name.includes("commits") || e.name.includes("P/F ratio")
    );
  }

  static initializeDataFromTokenTerminalPageFields(asset) {
    const regexForNumber = /^[^a-zA-Z]+$/;
    asset.dev_activity_monthly =
      asset.data_token_terminal?.[1]?.value && regexForNumber
        ? asset.data_token_terminal[1].value
        : "can't get dev activity monthly from token terminal page fields";

    asset.pf_ratio_fully_diluted =
      asset.data_token_terminal?.[0]?.value &&
      asset.data_token_terminal?.[0]?.name.toLowerCase() ==
        "p/f ratio (fully diluted)"
        ? asset.data_token_terminal[0].value
        : "can't get P/F ratio fully diluted from token terminal page fields";
  }

  static filterDataFromTokenTerminalPageFieldsTable(asset) {
    return asset.fees_token_terminal.filter((e) =>
      e.name.toLowerCase().includes(asset.name.toLowerCase())
    );
  }

  static initializeDataFromTokenTerminalPageFieldsTable(asset) {
    asset.fees_j_minus_1 = asset.fees_token_terminal?.[0]?.value
      ? asset.fees_token_terminal[0].value
      : "can't get fees from token terminal page fields table";
  }

  static clearUselessData(asset) {
    try {
      delete asset.data_token_terminal;
      delete asset.fees_token_terminal;
    } catch (e) {
      console.log("can't delete useless data");
    }
  }

  static clearUselessInterchainData(asset) {
    try {
      delete asset.currentDate;
      delete asset.yesterdayDate;
    } catch (e) {
      console.log("can't delete interchain useless data");
    }
  }
}
