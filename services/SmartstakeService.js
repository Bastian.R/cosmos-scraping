import { PageService } from "./PageService.js";

export class SmartstakeService extends PageService {
  static async deletePageNews(page) {
    return super.clickOnPageElement(
      page,
      "#___reactour > div:nth-child(4) > div > button > svg"
    );
  }
  static async getDataFromPage(page) {
    return await page.evaluate(() => {
      let unbonding;

      try {
        unbonding = document.querySelector(
          "#page-wrap > div > div:nth-child(1) > div.containerLayout.container-fluid > div > div:nth-child(4) > div > p"
        ).innerText;
      } catch (e) {
        console.log("can't get data from smartstake page");
      }

      unbonding?.includes("/")
        ? (unbonding = unbonding.split("/"))
        : "data get from smartstake page is not correct";

      return {
        unbonding_1_days: unbonding?.[0]
          ? unbonding[0]
          : "can't get unbonding_1_day from smartstake page",
        unbonding_7_days: unbonding?.[1]
          ? unbonding[1]
          : "can't get unbonding_7_days from smartstake page",
        unbonding_14_days: unbonding?.[2]
          ? unbonding[2]
          : "can't get unbonding_14_day from smartstake page",
      };
    });
  }
}
