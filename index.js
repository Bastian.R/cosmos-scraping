import puppeteer from "puppeteer";
import exceljs from "exceljs";

import {
  cosmos_data_url,
  osmosis_data_url,
  axelar_data_url,
} from "./config.js";

import { MapOfZonesService } from "./services/MapOfZonesService.js";
import { TokenTerminalService } from "./services/TokenTerminalService.js";
import { MintscanService } from "./services/MintscanService.js";
import { AxelarService } from "./services/AxelarService.js";
import { CoinMarketCapService } from "./services/CoinMarketCapService.js";
import { SmartstakeService } from "./services/SmartstakeService.js";
import { FormatDateService } from "./services/FormatDateService.js";
import { ExcelService } from "./services/ExcelService.js";
import { AssetService } from "./services/AssetService.js";

const get_data_from_browser = async (asset_data_url) => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: null,
    // devtools: true,
  });

  const page = await browser.newPage();

  let asset = {};

  if (asset_data_url.asset_url_mintscan) {
    await MintscanService.goToPage(page, asset_data_url.asset_url_mintscan);

    try {
      await MintscanService.waitForPageSelector(
        page,
        "div.contents.s-AlA33PZP3RDs > section > div > div > div:nth-child(6) > div > div > div > div:nth-child(2) > div > div:nth-child(2) > div"
      );
    } catch (e) {}

    await delay(3000);

    asset = await MintscanService.getDataFromPage(page);
  }

  if (asset_data_url.tvl_url_mintscan) {
    await MintscanService.goToPage(page, asset_data_url.tvl_url_mintscan);

    try {
      await MintscanService.waitForPageSelector(
        page,
        "#content-root > div.root.s-ZNy69WCvR5Me > div > div > div:nth-child(1) > div > div.contents.s-AlA33PZP3RDs > section > div > div > div:nth-child(1) > div:nth-child(4)"
      );
    } catch (e) {}

    await delay(3000);

    asset.tvl = await MintscanService.getDataFromPageAsset(page);
  }

  let total_market_cap;

  if (asset_data_url.dominance_url_mintscan) {
    await MintscanService.goToPage(page, asset_data_url.dominance_url_mintscan);

    try {
      await MintscanService.waitForPageSelector(
        page,
        "#content-root > div > div:nth-child(1) > div > div:nth-child(3) > section > div > div > div:nth-child(2) > div.tag-anim.s-dpAAwU3LLMp_ > div.text.s-dpAAwU3LLMp_ > div"
      );
    } catch (e) {}

    await delay(3000);

    total_market_cap = await MintscanService.getDataFromPageVisualization(page);
  }

  if (asset_data_url.asset_url_coinMarketCap) {
    await CoinMarketCapService.goToPage(
      page,
      asset_data_url.asset_url_coinMarketCap
    );

    try {
      await CoinMarketCapService.waitForPageSelector(
        page,
        "#section-coin-stats > div > dl > div:nth-child(7) > div > dd"
      );
    } catch (e) {}

    await delay(3000);

    let assetDataFromCoinMarketCap =
      await CoinMarketCapService.getDataFromPageCurrencies(page);

    asset = { ...asset, ...assetDataFromCoinMarketCap };
  }

  if (
    total_market_cap === "can't get asset total market cap on mintscan" ||
    !total_market_cap
  ) {
    asset.market_cap_dominance =
      "can't calculate market cap dominance due to a error getting total market cap on mintscan";
  } else {
    total_market_cap = AssetService.formatTotalMarketCap(total_market_cap);
    if (
      asset.market_cap ===
      "can't get asset market cap on page currencies of coinmarketcap"
    ) {
      asset.market_cap_dominance =
        "can't calculate market cap dominance due to a error getting asset marketcap on coinmarketcap";
    } else {
      let market_cap = AssetService.formatMarketCap(asset.market_cap);
      asset.market_cap_dominance = AssetService.calculateMarketCapDominance(
        market_cap,
        total_market_cap
      );
      asset.market_cap_dominance = AssetService.formatMarketCapDominance(asset);
    }
  }

  if (asset_data_url.ibc_url_mapofzones) {
    await MapOfZonesService.goToPage(page, asset_data_url.ibc_url_mapofzones);

    try {
      await MapOfZonesService.waitForPageSelector(
        page,
        ".ValueWithPending_valueContainer__ehTR7 > span > span"
      );
    } catch (e) {}

    await delay(3000);

    let valueFromMapOfZones = await MapOfZonesService.getDataFromPageField(
      page
    );

    asset = AssetService.initializeDataFromMapOfZonesPageField(
      asset,
      valueFromMapOfZones
    );
  }

  if (asset_data_url.code_commits_url_token_terminal) {
    await TokenTerminalService.goToPage(
      page,
      asset_data_url.code_commits_url_token_terminal
    );

    try {
      await TokenTerminalService.waitForPageSelector(
        page,
        "._1cidn1cfx._1cidn1c7n > section > ol > li > p"
      );
    } catch (e) {}

    await delay(3000);

    asset.data_token_terminal =
      await TokenTerminalService.getDataFromPageFields(page);

    asset.data_token_terminal =
      AssetService.filterDataFromTokenTerminalPageFields(asset);

    AssetService.initializeDataFromTokenTerminalPageFields(asset);
  }

  if (asset_data_url.fees_url_token_terminal) {
    await TokenTerminalService.goToPage(
      page,
      asset_data_url.fees_url_token_terminal
    );

    try {
      await TokenTerminalService.waitForPageSelector(
        page,
        "#main-content > div"
      );
    } catch (e) {}

    await delay(3000);

    try {
      await TokenTerminalService.clickOnPageElement(
        page,
        "#main-content > div > div._2rn9xx0._1cidn1ceb._1cidn1ceg._1cidn1cel._1cidn1ccz._1cidn1cd4._1cidn1cd9._1cidn1cgz._1cidn1ch4._1cidn1ch9._1cidn1cfn._1cidn1cfs._1cidn1cfx._1cidn1c3v._1cidn1crr._1cidn1c27._1cidn1c4v._1cidn1c8z.iv7w9e0.iv7w9e4.card._5bvu1x1._5bvu1x0 > div > div._2rn9xx0._1cidn1c3v._1cidn1cpv._1cidn1cbf._1cidn1cab._1cidn1cqn._1cidn1c73._1cidn1csb > div._2rn9xx0._1cidn1c3v._1cidn1cpv._1cidn1cbf._1cidn1cab._1cidn1cqn._1cidn1c6f > ol > li:nth-child(4) > span > button"
      );
    } catch (e) {
      console.log("can't click on page element on token terminal");
    }

    await delay(15000);

    asset.fees_token_terminal =
      await TokenTerminalService.getDataFromPageFieldsTable(page);

    asset.fees_token_terminal =
      AssetService.filterDataFromTokenTerminalPageFieldsTable(asset);

    AssetService.initializeDataFromTokenTerminalPageFieldsTable(asset);
  }

  if (asset_data_url.unbonding_url_smartstake) {
    await SmartstakeService.goToPage(
      page,
      asset_data_url.unbonding_url_smartstake
    );

    try {
      await SmartstakeService.waitForPageSelector(
        page,
        "#___reactour > div:nth-child(4) > div > button > svg"
      );
    } catch (e) {}

    await delay(3000);

    try {
      await SmartstakeService.deletePageNews(page);
    } catch (e) {}

    try {
      await SmartstakeService.waitForPageSelector(
        page,
        "#page-wrap > div > div:nth-child(1) > div.containerLayout.container-fluid > div > div:nth-child(4) > div > p"
      );
    } catch (e) {}

    await delay(3000);

    let valueFromSmartstake = await SmartstakeService.getDataFromPage(page);

    asset = { ...asset, ...valueFromSmartstake };
  }

  if (asset_data_url.total_mev_revenus_url_mintscan) {
    await MintscanService.goToPage(
      page,
      asset_data_url.total_mev_revenus_url_mintscan
    );
    try {
      await MintscanService.waitForPageSelector(
        page,
        "#content-root > div.root.s-ZNy69WCvR5Me > div > div.root.s-AlA33PZP3RDs > div.contents.s-AlA33PZP3RDs > div > div:nth-child(1) > div > div.contents.s-AlA33PZP3RDs > div > div:nth-child(3) > section > div > div > div > div:nth-child(2)"
      );
    } catch (e) {}

    await delay(3000);

    asset.total_mev_revenus = await MintscanService.getDataFromPageAddress(
      page
    );
  }

  if (asset_data_url.interchain_url_axelarscan) {
    let gasFees;

    await AxelarService.goToPage(
      page,
      asset_data_url.interchain_url_axelarscan
    );
    try {
      await AxelarService.waitForPageSelector(
        page,
        "#__next > div > div.wrapper > div > div:nth-child(3) > div > div > div:nth-child(2) > div > div:nth-child(3)"
      );
    } catch (e) {}

    await delay(3000);

    try {
      await AxelarService.clickOnPageElement(
        page,
        "#__next > div > div.wrapper > div > div:nth-child(3) > div > div > div:nth-child(2) > div > div:nth-child(3)"
      );
    } catch (e) {
      console.log("can't click on page element on token terminal");
    }

    await delay(10000);

    gasFees = await AxelarService.getDataFromPageInterchainGasFees(page);

    if (gasFees !== "can't get interchain gas fees on axelar") {
      if (
        asset.market_cap !==
        "can't get asset market cap on page currencies of coinmarketcap"
      ) {
        asset.price_fees_ratio =
          Number(AssetService.formatMarketCap(asset.market_cap)) /
          Number(AssetService.formatGasFees(gasFees));
        asset.price_fees_ratio = asset.price_fees_ratio.toFixed(2);
      } else {
        asset.price_fees_ratio =
          "can't calculate P/F ratio due to a error getting marketcap on coinmarketcap";
      }
    } else {
      asset.price_fees_ratio =
        "can't calculate P/F ratio due to a error getting gasFees on axelar interchain";
    }
  }

  if (asset.name.toLowerCase() === "axelar") {
    asset.currentDate = FormatDateService.createCurrentDate();
    asset.yesterdayDate = FormatDateService.createYesterdayDate();

    asset.currentDate = FormatDateService.extractDateData(asset.currentDate);
    asset.yesterdayDate = FormatDateService.extractDateData(
      asset.yesterdayDate
    );

    asset.currentDate = FormatDateService.formatMonthDate(asset.currentDate);
    asset.yesterdayDate = FormatDateService.formatMonthDate(
      asset.yesterdayDate
    );

    asset.currentDate = FormatDateService.formatDate(asset.currentDate);
    asset.yesterdayDate = FormatDateService.formatDate(asset.yesterdayDate);

    asset.currentDate = FormatDateService.formatDateToTimestamp(
      asset.currentDate
    );

    asset.yesterdayDate = FormatDateService.formatDateToTimestamp(
      asset.yesterdayDate
    );

    let interchain_24h_url_axelarscan = `https://axelarscan.io/interchain?period=24h&fromTime=${asset.yesterdayDate}&toTime=${asset.currentDate}`;

    await AxelarService.goToPage(page, interchain_24h_url_axelarscan);

    try {
      await AxelarService.waitForPageSelector(
        page,
        "#__next > div > div.wrapper > div > div:nth-child(3) > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div:nth-child(4) > div > div > div:nth-child(2) > span"
      );
    } catch (e) {}

    await delay(3000);

    let interchain_24h = await AxelarService.getDataFromPageInterchain24h(page);

    asset = { ...asset, ...interchain_24h };

    AssetService.clearUselessInterchainData(asset);
  }

  AssetService.clearUselessData(asset);

  await browser.close();

  let excelFile = ExcelService.createExcelFile(exceljs, asset);

  ExcelService.stylingRows(excelFile.ws);

  ExcelService.initializeColumns(
    excelFile.ws,
    ExcelService.extractColumnName(asset)
  );

  ExcelService.initializeCells(
    excelFile.ws,
    ExcelService.extractColumnName(asset),
    alphabet,
    asset
  );

  await ExcelService.saveExcelFile(
    excelFile.workbook,
    asset,
    ExcelService.initializeDate()
  );

  console.log(`Excel file about ${asset.name} asset saved, enjoy!`);
};

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

const delay = async (time) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
};

get_data_from_browser(cosmos_data_url);
get_data_from_browser(osmosis_data_url);
get_data_from_browser(axelar_data_url);
