# Cosmos Scraping

Projet pour la communauté les Cosmonautes ( 4000 membres ) concernant l'automatisation de récupération de données issues de l'écosystème crypto Cosmos pour analyse quotidienne.

https://www.youtube.com/@LesCosmonautes

https://discord.com/invite/GHErW8ytvY


## Tech Stack

Javascript, Node.js, Pupeeter, ExcelJS, Css Selector.


## Skills

- Création d'une application avec Node.js.
- Navigation automatisée sur le web avec la librairie Pupeeter.
- Récupération des données asynchrones avec la librairie Pupeeter & Css selector.
- Exportation des données dans divers fichiers Excel avec la librairie ExcelJS.
- Maintenance préventive afin de gérer les cas d'erreurs.
- Maintenance corrective avec du débogage client & serveur.
- Abstraction de la logique métier via classes & héritages.


## Data output example

https://drive.google.com/drive/folders/1uzPajXM9me67H-Zgme6GomeIIY47PeNi?usp=sharing


## License

Ce travail étant effectué pour un client privé il n'est en aucun cas partageable, modifiable éxécutable sans l'autorisation préalable de l'auteur & du client. Il est seulement partagé à but démonstratif par l'auteur & le client.


## Authors

- [@Bastian.R](https://gitlab.com/Bastian.R/)

