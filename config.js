const cosmos_data_url = {
  asset_url_mintscan: "https://www.mintscan.io/cosmos",
  tvl_url_mintscan: "https://www.mintscan.io/cosmos/assets",
  asset_url_coinMarketCap: "https://coinmarketcap.com/en/currencies/cosmos/",
  code_commits_url_token_terminal:
    "https://tokenterminal.com/terminal/projects/cosmos",
  fees_url_token_terminal: "https://tokenterminal.com/terminal",
  dominance_url_mintscan: "https://www.mintscan.io/visualization/dominance",
  ibc_url_mapofzones:
    "https://mapofzones.com/zones/cosmoshub-4/overview?columnKey=ibcVolume&period=24h",
  unbonding_url_smartstake: "https://analytics.smartstake.io/cosmos",
};

const osmosis_data_url = {
  asset_url_mintscan: "https://www.mintscan.io/osmosis",
  tvl_url_mintscan: "https://www.mintscan.io/osmosis/assets",
  asset_url_coinMarketCap: "https://coinmarketcap.com/en/currencies/osmosis/",
  code_commits_url_token_terminal:
    "https://tokenterminal.com/terminal/projects/osmosis",
  fees_url_token_terminal: "https://tokenterminal.com/terminal",
  dominance_url_mintscan: "https://www.mintscan.io/visualization/dominance",
  ibc_url_mapofzones:
    "https://mapofzones.com/zones/osmosis-1/overview?columnKey=ibcVolume&period=24h",
  unbonding_url_smartstake: "https://analytics.smartstake.io/osmosis",
  total_mev_revenus_url_mintscan:
    "https://www.mintscan.io/osmosis/address/osmo17qdmjdumw4xawam4g46gtwzle5rd4zwyfqvvza",
};

const axelar_data_url = {
  asset_url_mintscan: "https://www.mintscan.io/axelar",
  tvl_url_mintscan: "https://www.mintscan.io/axelar/assets",
  asset_url_coinMarketCap: "https://coinmarketcap.com/en/currencies/axelar/",
  dominance_url_mintscan: "https://www.mintscan.io/visualization/dominance",
  ibc_url_mapofzones:
    "https://mapofzones.com/zones/axelar-dojo-1/overview?columnKey=ibcVolume&period=24h",
  interchain_url_axelarscan: "https://axelarscan.io/interchain",
};

export { cosmos_data_url, osmosis_data_url, axelar_data_url };
